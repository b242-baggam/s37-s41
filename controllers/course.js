const Course = require("../models/Course");

// Create a new course
module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


module.exports.getAllActive = () => {
	return Course.find({ isActive: true}).then(result => {
		return result;
	})
}
// reqParams is a user defined parameter
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// we will use findByIdAndUpdate for updating the documents
module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description:reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) => {
		// Course is not updated
		if(error){
			return false;
		}
		else {
			return true;
		}
	})
}

module.exports.archiveCourse = (reqParams, data) => {
	let archivedCourse = {
		isActive : data.course.isActive
	}
	if(data.isAdmin){
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course,error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
	}
	else{
		return false;
	}
