const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

router.get("/all",(req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

router.get("/",(req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(
		resultFromController));
})

router.get("/:courseId", (req,res) =>{
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:courseId", auth.verify, (req,res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:courseId", auth.verify, (req,res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.body.authorization).isAdmin
	}

	courseController.archiveCourse(req.params,data).then(resultFromController => res.send(resultFromController));
})


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;