const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
// Allows access to routes defined within our application
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const app = express();

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://alvin-estiva:IHRqUSLj62pP3zBP@mongodb-cluster.0zh91.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
// Prompts a message in the terminal once the connection is "open"
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// Defines the "/users" string to be included for all user routes
app.use("/users", userRoutes);
// Defines the "/courses" string to be included for all course routes
app.use("/courses", courseRoutes);

// Port number definition
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000}`);
});